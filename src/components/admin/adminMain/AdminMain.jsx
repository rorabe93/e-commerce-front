import React from "react";
import MyRoutes from "../../../MyRoutes";

const AdminMain = () => {
  return (
    <>
      <MyRoutes type="admin" />
    </>
  );
};

export default AdminMain;
